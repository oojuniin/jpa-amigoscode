package br.com.jpa;

import br.com.jpa.model.Student;
import br.com.jpa.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application{
  public static void main(String[] args){
    SpringApplication.run(Application.class, args);
  }

  @Bean
  CommandLineRunner commandLineRunner(StudentRepository repository){
    return args->{
      Student maria=new Student("Maria", "Jones", "maria@gmail.com", 21);
      repository.save(maria);
    };
  }
}
